import axios, { AxiosPromise, AxiosRequestConfig } from 'axios';
import { env } from './env';
import { ItemData } from './types';

interface ItemsResponse {
  message: string;
  items: ItemData[];
}

interface SessionResponse {
  message: string;
  user: string;
}

interface VerifyTokenRequest {
  status: number;
  message: string;
  user: string;
}

export const sendRequest = async <T>(options: AxiosRequestConfig): Promise<AxiosPromise<T>> => axios(options);

export const verifyTokenRequest = async (token: string): Promise<VerifyTokenRequest> => {
  try {
    const response = await sendRequest<SessionResponse>({
      url: `http://${env.session.host}:${env.session.port}/api/session/verify/${token}`,
      method: 'GET',
    });
    return { status: response.status, message: response.data.message, user: response.data.user };
  } catch (error) {
    return { status: 500, message: error, user: '' };
  }
};

interface GetItemsRequest {
  status: number;
  message: string;
  items: ItemData[];
}

export const getItemsRequest = async (user: string): Promise<GetItemsRequest> => {
  try {
    const response = await sendRequest<ItemsResponse>({
      url: `http://${env.cartStore.host}:${env.cartStore.port}/api/cart/store/${user}`,
      method: 'GET',
    });
    return { status: response.status, message: response.data.message, items: response.data.items };
  } catch (error) {
    return { status: 500, message: error, items: [] };
  }
};

interface StoreItemRequest {
  status: number;
  message: string;
  items: ItemData[];
}

export const storeItemRequest = async (user: string, item: ItemData): Promise<StoreItemRequest> => {
  try {
    const response = await sendRequest<ItemsResponse>({
      url: `http://${env.cartStore.host}:${env.cartStore.port}/api/cart/add`,
      method: 'POST',
      data: {
        user,
        item,
      },
    });
    return { status: response.status, message: response.data.message, items: response.data.items };
  } catch (error) {
    return { status: 500, message: error, items: [] };
  }
};

interface DeleteItemRequest {
  status: number;
  message: string;
  items: ItemData[];
}

export const deleteItemRequest = async (user: string, item: ItemData): Promise<DeleteItemRequest> => {
  try {
    const response = await sendRequest<ItemsResponse>({
      url: `http://${env.cartStore.host}:${env.cartStore.port}/api/cart/rm`,
      method: 'POST',
      data: {
        user,
        item,
      },
    });
    return { status: response.status, message: response.data.message, items: response.data.items };
  } catch (error) {
    return { status: 500, message: error, items: [] };
  }
};

interface ClearItemsRequest {
  status: number;
  message: string;
  items: ItemData[];
}

export const clearItemsRequest = async (user: string): Promise<ClearItemsRequest> => {
  try {
    const response = await sendRequest<ItemsResponse>({
      url: `http://${env.cartStore.host}:${env.cartStore.port}/api/cart/store/${user}`,
      method: 'DELETE',
    });
    return { status: response.status, message: response.data.message, items: response.data.items };
  } catch (error) {
    return { status: 500, message: error, items: [] };
  }
};
