import Ajv from 'ajv';

const ajv = new Ajv();

ajv.addFormat('amount', {
  type: 'number',
  validate: (x) => x > 0 && isFinite(x),
});

const itemDataSchema = {
  type: 'object',
  properties: {
    item: {
      type: 'object',
      properties: {
        _id: { type: 'string' },
        amount: { type: 'number', format: 'amount' },
      },
      required: ['_id', 'amount'],
      additionalProperties: false,
    },
  },
  required: ['item'],
  additionalProperties: false,
};

export const validateItemData = ajv.compile(itemDataSchema);
