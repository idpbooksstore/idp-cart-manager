import { clearItemsRequest, deleteItemRequest, getItemsRequest, storeItemRequest, verifyTokenRequest } from './request';
import { ItemData } from './types';

export const getItems = async (token: string) => {
  const tokenResponse = await verifyTokenRequest(token);
  if (tokenResponse.status !== 200) return { status: tokenResponse.status, message: tokenResponse.message, items: [] };

  const getResponse = await getItemsRequest(tokenResponse.user);
  if (getResponse.status !== 200) return { status: getResponse.status, message: getResponse.message, items: [] };

  return { status: 200, message: 'Success', items: getResponse.items };
};

export const storeItem = async (token: string, item: ItemData) => {
  const tokenResponse = await verifyTokenRequest(token);
  if (tokenResponse.status !== 200) return { status: tokenResponse.status, message: tokenResponse.message, items: [] };

  const storeResponse = await storeItemRequest(tokenResponse.user, item);
  if (storeResponse.status !== 200) return { status: storeResponse.status, message: storeResponse.message, items: [] };

  return { status: 200, message: 'Success', items: storeResponse.items };
};

export const deleteItem = async (token: string, item: ItemData) => {
  const tokenResponse = await verifyTokenRequest(token);
  if (tokenResponse.status !== 200) return { status: tokenResponse.status, message: tokenResponse.message, items: [] };

  const deleteResponse = await deleteItemRequest(tokenResponse.user, item);
  if (deleteResponse.status !== 200)
    return { status: deleteResponse.status, message: deleteResponse.message, items: [] };

  return { status: 200, message: 'Success', items: deleteResponse.items };
};

export const clearItems = async (token: string) => {
  const tokenResponse = await verifyTokenRequest(token);
  if (tokenResponse.status !== 200) return { status: tokenResponse.status, message: tokenResponse.message, items: [] };

  const deleteResponse = await clearItemsRequest(tokenResponse.user);
  if (deleteResponse.status !== 200)
    return { status: deleteResponse.status, message: deleteResponse.message, items: [] };

  return { status: 200, message: 'Success', items: deleteResponse.items };
};
