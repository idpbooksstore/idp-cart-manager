import { clearItems, deleteItem, getItems, storeItem } from './service';
import Router, { Request, Response } from 'express';
import { ItemData } from './types';
import { env } from './env';
import { validateItemData } from './validate';

export const router = Router();

interface PostData {
  item: ItemData;
}

export const postCart = async (req: Request, res: Response) => {
  if (!validateItemData(req.body)) return res.status(400).send({ message: validateItemData.errors, items: [] });
  try {
    const { item } = req.body as PostData;
    const token = req.header(env.auth.token);
    if (token === undefined) return res.status(401).send({ message: 'Undefined Token', items: [] });

    const { status, message, items } = await storeItem(token, item);

    res.status(status).send({ message, items });
  } catch (error) {
    return res.status(500).send({ message: error, items: [] });
  }
};

export const getCart = async (req: Request, res: Response) => {
  try {
    const token = req.header(env.auth.token);
    if (token === undefined) return res.status(401).send({ message: 'Undefined Token', items: [] });

    const { status, message, items } = await getItems(token);

    res.status(status).send({ message, items });
  } catch (error) {
    return res.status(500).send({ message: error, items: [] });
  }
};

interface DeleteData {
  item: ItemData;
}

export const deleteCart = async (req: Request, res: Response) => {
  if (!validateItemData(req.body)) return res.status(400).send({ message: validateItemData.errors, items: [] });
  try {
    const { item } = req.body as DeleteData;
    const token = req.header(env.auth.token);
    if (token === undefined) return res.status(401).send({ message: 'Undefined Token', items: [] });

    const { status, message, items } = await deleteItem(token, item);

    res.status(status).send({ message, items });
  } catch (error) {
    return res.status(500).send({ message: error, items: [] });
  }
};

export const deleteAllCart = async (req: Request, res: Response) => {
  try {
    const token = req.header(env.auth.token);
    if (token === undefined) return res.status(401).send({ message: 'Undefined Token', items: [] });

    const { status, message, items } = await clearItems(token);

    res.status(status).send({ message, items });
  } catch (error) {
    return res.status(500).send({ message: error, items: [] });
  }
};

router.get('/', getCart);
router.post('/add', postCart);
router.post('/rm', deleteCart);
router.delete('/', deleteAllCart);
