# IDP Cart Store Manager

This module is used to save the state of the shopping cart.

Response statusCode:

- `200` when the data is valid and there are no errors
- `400` if the data is invalid
- `500` if there are errors at runtime

## Routes
- `GET /api/cart/`
  - Header: `{token: string}`
  - status: `200` - when the data is valid and there are no errors
  - status: `400` - if the data is invalid
  - status: `500` - if there are errors at runtime

- `POST /api/cart/add`
  - Body: `{item: {id: int, amount: int}}`
  - Header: `{token: string}`
  - status: `200` - when the data is valid and there are no errors
  - status: `400` - if the data is invalid
  - status: `500` - if there are errors at runtime

- `POST /api/cart/rm`
  - Body: `{item: {id: int, amount: int}}`
  - Header: `{token: string}`
  - status: `200` - when the data is valid and there are no errors
  - status: `400` - if the data is invalid
  - status: `500` - if there are errors at runtime

- `DELETE /api/cart/`
  - Header: `{token: string}`
  - status: `200` - when the data is valid and there are no errors
  - status: `400` - if the data is invalid
  - status: `500` - if there are errors at runtime

## Docker Container

This module can be built as a docker container.

#### Environment Variables

- `PORT` - The port the server will listen on. Default to `8080`
- `HOST` - Default to `localhost`
- `BOOKS_HOST` 
- `BOOKS_PORT`
- `CART_STORE_HOST`
- `CART_STORE_PORT`
- `SESSION_HOST`
- `SESSION_PORT`
- `NODE_ENV` - `production` or `development`

## Available Scripts

In the project directory, you can run:

### `npm run build`

First it runs the linter and the formatter.<br />
Builds the app for production to the `dist` folder.<br />

### `npm run start`

Runs the build from `dist`.<br />

### `npm run test`

Launches the test runner.<br />

### `npm run format`

Runs the formatter.<br />

### `npm run lint`

Runs the linter.<br />

### `npm run prebuild`

Runs the linter and the formatter.<br />

### `npm run build:live`

Runs the code in watch mode using nodemon and ts-node.<br />
